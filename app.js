const app = require("express")();
const http = require("http").Server(app);
const io = require('socket.io')(http);

io.on("connection", (socket) => {

    socket.on("new-message", message => {
        io.emit("message-list", {
            id: socket.id,
            msg: message,
            timestamp: Date.now()
        });
    });

});

http.listen(3000);